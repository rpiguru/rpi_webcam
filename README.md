## Change the background image

![Change Deskop Background](video/change_background.mp4 "Change Desktop Background")

## Set custom boot screen

Select your favourite image and copy it to `/etc/splash.png`.

That's all.

## Enable auto-start of an application.

Let's say that we need to execute an application at the booting time of RPi, and that application can be executed with the command below:
    
    cd /home/pi/my_application
    sudo ./my_application
    
This application is located at `/home/pi/my_application` and it can be executed with bash.

Now, open the `/etc/rc.local` and add below before `exit 0` line:
    
    (cd /home/pi/my_application; bash my_application)&
    
![Enable Auto Start](img/enable_autostart.jpg "Enable Auto Start")


**NOTE**:
 
- `&` should be added at the end of line.
- Before executing, we need to move to its existing directory with `/home/pi/my_application`.
  Because without this, the application will be executed from `/root` directory, and some applications will not work if it contains relative path in it.
  

## Run Kivy app
    
    cd kivy_app
    sudo python app.py


## Run Kivy Demo app
    
    cd ~
    cd work/kivymd/kivymd
    sudo python demo.py
